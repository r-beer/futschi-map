# Epected data format

This is a description of the expected data format for the bullets pins displaying one #futschi incident. The website will process an Array of these objects.


```js
{
  // company's name
  company: String,
  // the pin will show this image, if defined
  imageURL: ?String,
  // industry sector company operates in; may also be omitted
  industry: ?String,
  // number of jobs lost
  layoffs: Number,
  // longitude and latitude are real world GPS coordinates
  latitude: Number,
  longitude: Number
}
```

Example from `/res/elchin-data.xlsx`

| Firma    | Standort | Mitarbeiteranzahl | Insolvenz | Auslagerung | Abbau | Branche     |
|:--------:|:--------:|:-----------------:|:---------:|:-----------:|:-----:|:-----------:|
| Leverton | Berlin   | 52                | nein      | nein        | ja    | Technologie |


```json
{
  "company": "Leverton",
  "layoffs": 52,
  "industry": "Technologie",
  "latitude": 52.520007,
  "longitude": 13.404954
}
```

There will be the question of how to handle layoffs happening all over the country, e.g. Deutsche Bank closing several subsidiaries. I suggest we use the company's headquarter location whenever no detailed data is available.
