import { MapImageSeries } from '@amcharts/amcharts4/maps';
import { Ellipse, Circle, color } from '@amcharts/amcharts4/core';
import { OverlapBuster } from '@amcharts/amcharts4/plugins/overlapBuster';

const createImageSeries = (imageData) => {
  const imageSeries = new MapImageSeries();
  const imageTemplate = imageSeries.mapImages.template;
  imageTemplate.propertyFields.longitude = 'longitude';
  imageTemplate.propertyFields.latitude = 'latitude';
  imageTemplate.nonScaling = false;
  // make objects measurable, for OverlapBuster plugin to work
  imageTemplate.layout = 'absolute';
  imageTemplate.isMeasured = true;

  // Creating a pin bullet
  const circle = imageTemplate.createChild(Circle);

  // Set what to display on rollover tooltip
  circle.tooltipText = '{title}: [bold]{value}[/]';
  imageSeries.tooltip.pointerOrientation = 'top';

  // Configuring circle appearance
  circle.fill = color('#e23212');
  circle.stroke = color('#f33212');
  circle.fillOpacity = 0.7;

  imageSeries.heatRules.push({
   'target': circle,
   'property': 'radius',
   'min': 1,
   'max': 7,
   'dataField': 'value'
  });

  imageSeries.data = imageData;
  return { imageSeries, circle };
}

export default (map, imageData) => {
  const groupedByLat = imageData.reduce((series, data) => {
    if (!series[data.latitude]) {
      series[data.latitude] = [];
    }
    series[data.latitude].push(data);
    return series;
  }, {});

  let imageSeries = [];
  for (const lat in groupedByLat) {
    const series = createImageSeries(groupedByLat[lat]);
    // use OverlapBuster plugin
    const overlap = map.plugins.push(new OverlapBuster({
      tolerance: 1,
      revealRatio: 1
    }));
    imageSeries.push(series.imageSeries)
    map.series.push(series.imageSeries);
    overlap.targets.push(series.circle);
  }

  return imageSeries;
}
