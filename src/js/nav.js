import debug from 'debug';

import pinData from './layoffs.json';

const log = debug('fmap:nav');

export default class Navigation {
  static init(search) {
    const autoCompletionData = pinData.reduce((acData, pin) => ({
      ...acData,
      [pin.title]: null
    }), {});

    const searchElement = document.getElementById('search');
    const searchForm = document.getElementById('searchForm');

    const inst = M.Autocomplete.init(searchElement, {
      data: autoCompletionData,
      onAutocomplete: search
    });

    searchForm.addEventListener('submit', (evt) => {
      evt.preventDefault();
      search(searchElement.value);
    });
  }
}
