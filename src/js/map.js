import { color, create } from '@amcharts/amcharts4/core';
import { PinBullet } from '@amcharts/amcharts4/plugins/bullets';
import { MapChart, projections, MapPolygonSeries } from '@amcharts/amcharts4/maps';

import am4geodata_germanyLow from './germanyLow';

export default (pinData) => {
  const map = create('map', MapChart);
  map.geodata = am4geodata_germanyLow;
  map.projection = new projections.Miller();

  const polygonSeries = new MapPolygonSeries();
  polygonSeries.useGeodata = true;
  map.series.push(polygonSeries);

  const polygonTemplate = polygonSeries.mapPolygons.template;

  // map.homeZoomLevel = 1;
  // map.homeGeoPoint = { longitude: 10, latitude: 51 };

  return map;
};
