import 'materialize-css';
import debug from 'debug';
import { useTheme } from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import Navigation from './nav';
import createMap from './map';
import addPins from './pins';

import 'materialize-css/sass/materialize.scss';
import '../scss/style.scss';

import pinData from './layoffs.json';

useTheme(am4themes_animated);


const log = debug('fmap:main');
const mapElement = document.getElementById('map');

log('Creating map');
const map = createMap(pinData);

log('Adding PinBullets');
const pinMapSeries = addPins(map, pinData);

log('Showing');
mapElement.classList.remove('loading');

Navigation.init((choice) => {
  log(choice);
  const pins = pinMapSeries.mapImages.values;
  log(pinMapSeries.mapImages)
  pins.forEach(pin => {
    if (pin.dataItem.dataContext.title === choice) {
      pin.show();
    } else {
      pin.hide();
    }
  });
});
