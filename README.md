# Map of Germany displaying job cuts (#futschi)

## Install

Nothing special; simply clone and install dependencies
```bash
git clone https://bitbucket.org/r-beer/futschi-map.git
npm i
```

## Development

Build chain uses webpack. Configuration is in `/webpack.config.js`. For a development server (default: http://127.0.0.1:9000) and live updates run `npm run dev`.

For a development build (to /dist/) run `npm run build:dev`

## Build/Production

For a production build (also to /dist/) run `npm run build`

## ENV

You can change address and port the development server listens on by creating
a `.env.development.local` file with values for `FUTSCHI_MAP_DEV_SERVER_PORT` and `FUTSCHI_MAP_DEV_SERVER_HOST`, e.g.

```
FUTSCHI_MAP_DEV_SERVER_PORT=0.0.0.0
FUTSCHI_MAP_DEV_SERVER_HOST=3000
```
to make the the server listen on all available interfaces and port 3000

Your .env.* files will not be committed.
